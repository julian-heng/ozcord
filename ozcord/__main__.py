#!/usr/bin/env python3
# SPDX-License-Identifier: (GPL-3.0-only)
# Copyright (C) 2021 Julian Heng

""" Main entry point for the bot """

import argparse
import logging
import sys

from ozcord.core.bot import OzCordBot


def main(args):
    """ Main entry point """
    # Process command line arguments
    config = parse_args(args)
    logging.basicConfig(level=logging.DEBUG)

    if not config.token:
        sys.exit(1)

    bot = OzCordBot(command_prefix=config.prefix)
    bot.setup()
    bot.run(config.token)


def parse_args(args):
    """ Parses command line arguments """
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--token", action="store", type=str)
    parser.add_argument("-p", "--prefix", action="store", type=str,
                        default="ozb!")
    return parser.parse_args(args)


if __name__ == "__main__":
    main(sys.argv[1:])
