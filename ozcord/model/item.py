#!/usr/bin/env python3
# SPDX-License-Identifier: (GPL-3.0-only)
# Copyright (C) 2021 Julian Heng

# pylint: disable=invalid-name
# pylint: disable=too-many-arguments
# pylint: disable=too-many-branches
# pylint: disable=too-many-instance-attributes

""" Model classes for OzCord """


class OzItem():
    """
    Represents an item entry within OzBargain

    Attributes
    ----------
    title : str
        The post title
    link : str
        The link to the deal
    author : str
        The author who posted the post
    meta_link : str
        The link to the OzBargain post
    comment_count : int
        The number of comments on the post
    click_count : int
        The number of clicks on the post
    date_posted : str
        The time and date of the post when posted
    date_expire : str
        The expiry time of the deal
    votes_pos : int
        The number of positive votes on the psot
    votes_neg : int
        The number of negative votes on the post

    Methods
    -------
    from_element(elem)
        Creates an OzItem from an Element node in an ElementTree object

    """

    def __init__(
        self, title, link, author, meta_link, comment_count, click_count,
        date_posted, date_expire, votes_pos, votes_neg
    ):
        self.title = title
        self.link = link
        self.author = author
        self.meta_link = meta_link
        self.comment_count = comment_count
        self.click_count = click_count
        self.date_posted = date_posted
        self.date_expire = date_expire
        self.votes_pos = votes_pos
        self.votes_neg = votes_neg

    def __repr__(self):
        return (
            f"{self.title}\n"
            f"Posted by {self.author} on {self.date_posted} [{self.votes_pos}↑"
            f"|{self.votes_neg}↓]\n"
            f"Expires on {self.date_expire}\n"
            f"{self.click_count} Clicks, {self.comment_count} Comments\n"
            f"\n"
            f"{self.link}\n"
            f"{self.meta_link}"
        )

    def __str__(self):
        return self.__repr__()

    @staticmethod
    def from_element(elem):
        """Creates an OzItem from an Element node in an ElementTree object

        Parameters
        ----------
        elem : ElementTree.Element
            An item xml node from the rss feed

        Returns
        -------
        item : OzItem
            An OzItem object containing the data from the xml node
        """
        title = None
        link = None
        author = None
        meta_link = None
        comment_count = None
        click_count = None
        date_posted = None
        date_expire = None
        votes_pos = None
        votes_neg = None

        item = None

        for i in elem.iter():
            if i.tag == "title":
                title = i.text
            elif i.tag == "link":
                meta_link = i.text
            elif i.tag == "{https://www.ozbargain.com.au}meta":
                for k, v in i.attrib.items():
                    if k == "comment-count":
                        comment_count = int(v)
                    elif k == "click-count":
                        click_count = int(v)
                    elif k == "expiry":
                        date_expire = v
                    elif k == "votes-pos":
                        votes_pos = int(v)
                    elif k == "votes-neg":
                        votes_neg = int(v)
                    elif k == "url":
                        link = v
            elif i.tag == "pubDate":
                date_posted = i.text
            elif i.tag == "{http://purl.org/dc/elements/1.1/}creator":
                author = i.text

        item = OzItem(
            title, link, author, meta_link, comment_count, click_count,
            date_posted, date_expire, votes_pos, votes_neg
        )

        return item
