#!/usr/bin/env python3
# SPDX-License-Identifier: (GPL-3.0-only)
# Copyright (C) 2021 Julian Heng

""" The query cog """

import asyncio
import re

from xml.etree import ElementTree

import requests

from discord.ext import commands

from ozcord.model.item import OzItem


class OzCordQuery(commands.Cog):

    BASE_URL = "https://www.ozbargain.com.au"
    PRICE_REGEX = r"(\$(?:\d+(?:(?:,\d+)+)?(?:.\d\d)?))"
    VOTE_REGEX = r"(\d+(?:↑|↓))"

    @commands.command()
    async def tag(self, ctx, *args):
        if not args:
            # No args given
            await ctx.send("No search term given...")
            return

        # Tag allows multiple keywords for query
        search = "+".join(args)
        url = f"{OzCordQuery.BASE_URL}/tag/{search}/feed"
        await OzCordQuery._query(ctx, url)

    @commands.command()
    async def cat(self, ctx, *args):
        # Category only allows a single keyword for query
        search = next(iter(args), None)
        if search is None:
            # No args given
            await ctx.send("No search term given...")
            return

        url = f"{OzCordQuery.BASE_URL}/cat/{search}/feed"
        await OzCordQuery._query(ctx, url)

    @staticmethod
    async def _query(ctx, url, limit=5):
        # Fetch the rss feed
        try:
            response = requests.get(url)
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            await ctx.send(f"Error fetching page: {e}")
            return

        # Parse data
        tree = ElementTree.fromstring(response.content)

        # Loop through each item and print
        for n, i in enumerate(map(OzItem.from_element, tree.iter("item")), 1):
            if n > limit:
                break

            out = f"{n}. {i}"
            out = re.sub(OzCordQuery.PRICE_REGEX, r"**\1**", out)
            out = re.sub(OzCordQuery.VOTE_REGEX, r"**\1**", out)
            await ctx.send(out)
            await asyncio.sleep(0.1)
