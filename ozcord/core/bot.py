#!/usr/bin/env python3
# SPDX-License-Identifier: (GPL-3.0-only)
# Copyright (C) 2021 Julian Heng

""" The core bot """

import logging

from discord.ext import commands

from ozcord.core.query import OzCordQuery


class OzCordBot(commands.Bot):

    def setup(self):
        cogs = [
            OzCordQuery,
        ]

        for cog in cogs:
            self.add_cog(cog(self))

    async def on_ready(self):
        logging.log(logging.INFO, "Successfully logged in")

    async def on_msg(self, msg):
        if msg.author.bot:
            return
        await self.process_commands(msg)
